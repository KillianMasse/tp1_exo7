# -*- coding: utf-8 -*-
"""
Created on Mon Mar 23 12:43:34 2020
@author: Killian Massé

Ajout de test sur les types
"""


class SimpleCalculator:
    """
    Classe des opérations
    """
    def somme(self, nombre1, nombre2):
        """
        fonction somme
        """
        if isinstance(nombre1,int) and isinstance(nombre2,int):
            return nombre1 + nombre2
        else:
            return "ERROR"

    def soustraction(self, nombre1, nombre2):
        """
        fonction soustraction
        """
        if isinstance(nombre1,int) and isinstance(nombre2,int):
            return nombre1 - nombre2
        else:
            return "ERROR"

    def multiplication(self, nombre1, nombre2):
        """
        fonction multiplication
        """
        if isinstance(nombre1,int) and isinstance(nombre2,int):
            return nombre1 * nombre2
        else:
            return "ERROR"

    def division(self, nombre1, nombre2):
        """
        fonction division
        """
        if isinstance(nombre1,int) and isinstance(nombre2,int):
            if nombre2 == 0:
                raise ZeroDivisionError("Connot divide by zero")
            else:
                return nombre1 / nombre2
        else:
            return "ERROR"

if __name__ == "__main__":
    """
    Simple test in case of main call
    """
    VAL_A = 130
    VAL_B = 23
    RES = SimpleCalculator()  # creation d'un objet de ma classe
    print(RES.somme(VAL_A, VAL_B))
    print(RES.soustraction(VAL_A, VAL_B))
    print(RES.multiplication(VAL_A, VAL_B))
    print(RES.division(VAL_A, VAL_B))
