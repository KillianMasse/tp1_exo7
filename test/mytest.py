# -*- coding: utf-8 -*-

import unittest
import logging

from Calculator.simplecalculator import SimpleCalculator as SimpleCalculator


class AdditionTest(unittest.TestCase):
    """
    Classe addition gérée avec unittest
    """
    def setUp(self):
        """ Executed before every test case """
        self.Calculator = SimpleCalculator()

    def test_somme(self):
        result = self.Calculator.somme(30, 5)
        self.assertEqual(result, 35)
        logging.warning("test_addition ok warn")
        logging.info("test_addition ok info")


class SoustractionTest(unittest.TestCase):
    """
    Classe soustraction gérée avec unittest
    """
    def setUp(self):
        """ Executed before every test case """
        self.Calculator = SimpleCalculator()

    def test_soustraction(self):
        result = self.Calculator.soustraction(30, 5)
        self.assertEqual(result, 25)
        logging.warning("test_soustraction ok warn")
        logging.info("test_soustraction ok info")


class MultiplicationTest(unittest.TestCase):
    """
    Classe multiplication gérée avec unittest
    """
    def setUp(self):
        """ Executed before every test case """
        self.Calculator = SimpleCalculator()

    def test_multiplication(self):
        result = self.Calculator.multiplication(30, 5)
        self.assertEqual(result, 150)
        logging.warning("test_multiplication ok warn")
        logging.info("test_multiplication ok info")


class DivisionTest(unittest.TestCase):
    """
    Classe division gérée avec unittest
    """
    def setUp(self):
        """ Executed before every test case """
        self.Calculator = SimpleCalculator()

    def test_division(self):
        result = self.Calculator.division(30, 5)
        self.assertEqual(result, 6)
        logging.warning("test_division ok warn")
        logging.info("test_division ok info")


if __name__ == "__main__":
    unittest.main()
